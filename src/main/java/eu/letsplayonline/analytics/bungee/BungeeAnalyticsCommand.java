package eu.letsplayonline.analytics.bungee;

import eu.letsplayonline.analytics.core.Analytics;
import java.util.UUID;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 *
 * @author Julian Fölsch
 */
public class BungeeAnalyticsCommand extends Command {

    public BungeeAnalyticsCommand() {
        super("BAnalytics");
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        if (args[0] == null || (!"opt-out".equals(args[0]) && !"opt-in".equals(args[0]))) {
            // No args or invalid args
            TextComponent msg = new TextComponent("Please give valid arguments!");
            msg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("opt-in or opt-out").create()));
            commandSender.sendMessage(msg);
            return;
        }

        SQLiInteraction sqli = SQLiInteraction.getInstance();
        if (commandSender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) commandSender;
            UUID playerUUID = player.getUniqueId();
            String playerVersion = String.valueOf(player.getPendingConnection().getVersion());

            if ("opt-out".equals(args[0])) {
                if (SQLiInteraction.isOptOut(playerUUID) == false) {
                    if (sqli.handle("opt-out", player.getUniqueId())) {
                        Analytics.getInstance().leave(EntityFactory.getEntity(player, player.getServer().getInfo()
                                .getName(), playerVersion));
                        Listeners.setSessionOptOut(player.getUniqueId(), true);
                        commandSender.sendMessage(new TextComponent("Analytics opt-out successful!"));
                    } else {
                        commandSender.sendMessage(new TextComponent("Analytics opt-out failed!"));
                    }
                } else {
                    commandSender.sendMessage(new TextComponent("You have already opted out of Analytics!"));
                }

            } else {
                if (SQLiInteraction.isOptOut(playerUUID) == true) {
                    if (sqli.handle("opt-in", player.getUniqueId())) {
                        Analytics.getInstance().join(EntityFactory.getEntity(player, player.getServer().getInfo()
                                .getName(), playerVersion));
                        commandSender.sendMessage(new TextComponent("Analytics opt-in successful!"));
                        Listeners.setSessionOptOut(player.getUniqueId(), false);
                    } else {
                        commandSender.sendMessage(new TextComponent("Analytics opt-in failed!"));
                    }
                } else {
                    commandSender.sendMessage(new TextComponent("You are already opted in to Analytics!"));
                }
            }
        }
    }
}
