package eu.letsplayonline.analytics.bungee;

import net.md_5.bungee.api.plugin.Plugin;
import eu.letsplayonline.analytics.core.Analytics;

/**
 *
 * @author Julian Fölsch
 * @author Johannes Frank
 */
public class Main extends Plugin {

    @Override
    public void onEnable() {
        FileInteraction.startup(this);
        SQLiInteraction sqli = SQLiInteraction.getInstance();
        sqli.initDBConnection(this);
        Analytics.configure(FileInteraction.properties
                .getProperty("hostname"), FileInteraction.properties
                .getProperty("tracking-id"));
        getProxy().getScheduler().runAsync(this, new Runnable() {

            @Override
            public void run() {
                Analytics.start();
            }
        });
        getProxy().getPluginManager().registerCommand(this, new BungeeAnalyticsCommand());
        getProxy().getPluginManager().registerListener(this, new Listeners());
    }

    @Override
    public void onDisable() {
        getProxy().getPluginManager().unregisterListeners(this);
        getProxy().getPluginManager().unregisterCommands(this);
        SQLiInteraction.getInstance().shutdownDBConnection();
        getProxy().getScheduler().runAsync(this, new Runnable() {

            @Override
            public void run() {
                Analytics.stop();
            }

        });
    }
}
