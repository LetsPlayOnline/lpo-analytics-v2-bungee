package eu.letsplayonline.analytics.bungee;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import eu.letsplayonline.analytics.core.util.Entity;
import eu.letsplayonline.analytics.core.util.EntityType;

public class EntityFactory {

    public static Entity getEntity(ProxiedPlayer player, String servername,
            String version) {
        return new Entity(player.getUniqueId(), player.getName(), player
                .getAddress().getAddress().getHostAddress(), servername, version,
                EntityType.PLAYER);
    }
}
