package eu.letsplayonline.analytics.bungee;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;
import java.util.logging.Level;
import net.md_5.bungee.api.plugin.Plugin;

/**
 *
 * @author Julian Fölsch
 */
public class SQLiInteraction {

    private static final SQLiInteraction sqliInteraction = new SQLiInteraction();
    private static Connection connection;
    private static final String DB_PATH = "plugins/LPOAnalytics/database.db";
    private static Plugin hostPlugin;

    private SQLiInteraction() {

    }

    public static SQLiInteraction getInstance() {
        return sqliInteraction;
    }

    void initDBConnection(Plugin hostPlugin) {
        SQLiInteraction.hostPlugin = hostPlugin;
        try {
            Class.forName("org.sqlite.JDBC");
            try {
                if (connection != null) {
                    return;
                }
                hostPlugin.getLogger().log(Level.INFO, "Creating Connection to Database...");
                connection = DriverManager.getConnection("jdbc:sqlite:" + DB_PATH);
                if (!connection.isClosed()) {
                    hostPlugin.getLogger().log(Level.INFO, "...Connection established");
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            try {
                Statement stmt = connection.createStatement();
                stmt.executeUpdate("CREATE TABLE IF NOT EXISTS [status] ([UUID] CHAR(36) NOT NULL, [ACTIVE] boolean, CONSTRAINT [] PRIMARY KEY ([UUID]));");
            } catch (SQLException e) {
                hostPlugin.getLogger().log(Level.SEVERE, "Could not handle DB-Qery {0}", e);
            }
        } catch (ClassNotFoundException e) {
            hostPlugin.getLogger().log(Level.SEVERE, "Error loading JDBC-Driver {0}", e);
        }

    }

    boolean handle(String what, UUID who) {
        if ("opt-out".equals(what)) {
            try {
                PreparedStatement stmt0 = connection.prepareStatement("INSERT OR IGNORE INTO status (UUID, ACTIVE) VALUES (?,?)");
                stmt0.setString(1, who.toString());
                stmt0.setBoolean(2, true);
                return stmt0.executeUpdate() == 1;
            } catch (SQLException e) {
                hostPlugin.getLogger().log(Level.SEVERE, "Could not handle DB-Qery {0}", e);
            }
        } else {
            try {
                PreparedStatement stmt0 = connection.prepareStatement("DELETE FROM status WHERE UUID = ?");
                stmt0.setString(1, who.toString());
                return stmt0.executeUpdate() == 1;
            } catch (SQLException e) {
                hostPlugin.getLogger().log(Level.SEVERE, "Could not handle DB-Qery {0}", e);
            }
        }
        return false;
    }

    static boolean isOptOut(UUID who) {
        boolean optOut = false;
        try {
            PreparedStatement stmt0 = connection.prepareStatement("SELECT UUID, ACTIVE FROM status WHERE UUID = ?");
            stmt0.setString(1, who.toString());
            ResultSet rs = stmt0.executeQuery();
            if (rs.next()) {
                optOut = rs.getBoolean("ACTIVE");
            }
        } catch (SQLException e) {
            hostPlugin.getLogger().log(Level.SEVERE, "Could not handle DB-Query{0}", e);
        }
        return optOut;
    }

    void shutdownDBConnection() {
        try {
            if (!connection.isClosed() && connection != null) {
                connection.close();
                if (connection.isClosed()) {
                    hostPlugin.getLogger().log(Level.INFO, "Connection to Database closed");
                }
            }
        } catch (SQLException e) {
            hostPlugin.getLogger().log(Level.SEVERE, "{0}", e);
        }
    }

}
